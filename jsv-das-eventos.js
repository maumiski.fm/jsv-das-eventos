//1- Escribir un archivo con extensión .js donde se declare un vector de 6 elementos, usando 3 tipos de datos JS
let vector = ["Primer elemento", "Segundo elemento", 1, 2, true, false];

//a. Imprimir en la consola el vector
console.log(vector);

//b. Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log(vector[0]);
console.log(vector[vector.length-1]);

//c. Modificar el valor del tercer elemento
vector[2] = 3;

//d. Imprimir en la consola la longitud del vector
console.log(vector.length);

//e. Agregar un elemento al vector usando "push"
vector.push(6);

//f. Eliminar elemento del final e imprimirlo usando "pop"
console.log(vector.pop());

//g. Agregar un elemento en la mitad del vector usando "splice"
vector.splice(Math.round(vector.length/2),0,'Elemento medio');

//h. Eliminar el primer elemento usando "shift"
let elementoEliminado = vector.shift();

//i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(elementoEliminado);

/*--------------------------------------------------------------------------------------------------------------*/
//2- Escribir un archivo con extensión .js donde se declare un vector de 6 elementos, usando 3 tipos de datos JS:
let vector = ["Primer elemento", "Segundo elemento", 1, 2, true, false];

//a. Imprimir en la consola cada valor usando "for"
for (i = 0; i < vector.length; i++){
    console.log(vector[i])
}

//b. Idem al anterior usando "forEach"
vector.forEach(i => console.log(i));

//c. Idem al anterior usando "map"
vector.map(function(valor,indice) {
    console.log(vector[indice]);
});

// d. Idem al anterior usando "while"
let w = 0;
while (w < vector.length) {
    console.log(vector[w]);
    w++;
}

//e. Idem al anterior usando "for..of"
let n = 0;
for (i of vector) {
  console.log(vector[n]);
  n++;
}
